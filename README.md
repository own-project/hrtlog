# HRT data logging project

## Introduction

This project can be used to track your hormone levels from lab reports over time. 
It uses Dash, the Python web graph library and Flask to do so.

## Data structure

TBD

## The `.env` file

Copy `.env.example` to `.env` and edit whatever you need.

There are some settings in there that currently are not used but might be needed if you would like to put this app behind an nginx reverse proxy with a lets-encrypt-companion:
- HOSTNAME, EMAIL: used for registering the domain name with Let's Encrypt
- LEC_VERSION: The Let's Encrypt Companion docker image version to be used
- NP_VERSION: The nginx reverse proxy container version

I didn't add those two to the `docker-compose.yml` since I don't use them locally.

## Demo page

A demo deployment is here: http://hrtlog.lost-in-transition.lgbt