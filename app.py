import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_table
import plotly.express as px
import pandas as pd

app = dash.Dash(__name__)

df = pd.read_csv('data/data.csv')
fig = px.line(df, x="Date", y=["Testosterone", "Estradiol", "Prolactin"])

app.layout = html.Div(children=[
    html.H1(children='HRTlog'),

    html.Div(children='''
        HRTlog - See what's going on in your body … or better mine
    '''),

    dcc.Graph(
        id='example-graph',
        figure=fig
    ),

    dash_table.DataTable(
        id='table',
        data=df.to_dict('records'),
    ),

    html.Div(children='''
        Find the source code here: https://gitlab.com/own-project/hrtlog
    ''')
])

if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port=80, debug=True)
